'use strict';

/* jasmine specs for services go here */

describe('service', function() {
  beforeEach(module('teckovany.services'));
  var service;

  describe('PriceCalculator', function() {
    var baseProduct;
    beforeEach(function() {
      inject(function(PriceCalculator) {
        service = PriceCalculator;

        baseProduct = {
          pageSize: 'A4',
          numberOfPages: 50,
          hardCover: true,
          color: 'black',
          textColor: 'bright-yellow'
        };

      });
    });

    it('should return 129 for base product', function() {
      expect(service.calculate(baseProduct)).toEqual(129);
    });

    it('should return 151 when i define my favorite product', function() {
      var favoriteProduct = {
        pageSize: 'SQUARE',
        numberOfPages: 70,
        hardCover: true,
        color: 'black',
        textColor: 'bright-yellow',
        corner: true,
        rubber: true
      };

      var price = service.calculate(favoriteProduct);

      expect(price).toEqual(151);
    });

    it('should increase price when add corner', function() {
      var priceWithoutCorner = service.calculate(baseProduct);
      baseProduct.corner = true;
      var priceWithCorner = service.calculate(baseProduct);

      expect(priceWithCorner - priceWithoutCorner).toEqual(5);
    });
  });
});