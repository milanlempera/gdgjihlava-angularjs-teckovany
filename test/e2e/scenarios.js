
'use strict';

/* https://github.com/angular/protractor/blob/master/docs/getting-started.md */

describe('teckovany.cz', function() {

  beforeEach(function() {
    browser.get('index.html');
  });

  describe('product', function() {

    it('default product price', function() {
      expect(element(by.id('product-price')).getText()).toMatch(129);
    });

    it('shold calculate price', function() {
      element.all(by.model('product.pageSize')).last().click();
      element.all(by.model('product.hardCover')).last().click();
      element(by.model('product.corner')).click();
      element(by.model('product.rubber')).click();

      expect(element(by.id('product-price')).getText()).toMatch(130);
    });

  });

  describe('cart', function() {

    it('shold increase product count in one line when product is same', function() {
      element(by.css('.add-to-cart')).click();
      expect(element(by.css('.cart-product-count')).getAttribute('value')).toEqual('1');
      expect(element.all(by.css('.cart .products-list tr')).count()).toEqual(1);

      element(by.css('.add-to-cart')).click();
      expect(element(by.css('.cart-product-count')).getAttribute('value')).toEqual('2');
      expect(element.all(by.css('.cart .products-list  tr')).count()).toEqual(1);
    });

    it('shold increase lines count when products are diferent', function() {

      element(by.css('.add-to-cart')).click();
      expect(element(by.css('.cart-product-count')).getAttribute('value')).toEqual('1');
      expect(element.all(by.css('.cart .products-list  tr')).count()).toEqual(1);

      element.all(by.model('product.hardCover')).last().click();
      element(by.css('.add-to-cart')).click();

      expect(element.all(by.css('.cart-product-count')).first().getAttribute('value')).toEqual('1');
      expect(element.all(by.css('.cart-product-count')).last().getAttribute('value')).toEqual('1');
      expect(element.all(by.css('.cart .products-list tr')).count()).toEqual(2);

    });

  });

  describe('order', function() {

    it('shold be send', function() {
      element(by.css('.add-to-cart')).click();
      element(by.model('order.name')).sendKeys('Protractor tester');
      element(by.model('order.street')).sendKeys('test');
      element(by.model('order.city')).sendKeys('test');
      element(by.model('order.zip')).sendKeys('12345');
      element(by.model('order.email')).sendKeys('protractor@tester');
      element(by.model('order.aggree')).click();
      element(by.css('button.send')).click();

      expect(element(by.css('.message')).getText()).toMatch('Objednávka byla úspěšně odeslána');
    });

  });

});