'use strict';

/* Controllers */

angular.module('admin.controllers', [])
  .controller('OrdersCtrl',
    function($scope, Orders) {
      
      $scope.orders = Orders.query();
      
      $scope.statuses = {
        new  : 'Nová',
        made : 'Vyrobená',
        cancelled : 'Zrušená',
        paid : 'Zaplacená',
        sent : 'Odeslaná'
      };
      
      $scope.removeOrder = function(order) {
          order.$remove( function() {
            var index = $scope.orders.indexOf(order);
            $scope.orders.splice(index,1);              
          })
      };
      
    })
  .controller('DetailCtrl',
    function($scope, $routeParams, detail) {
      $scope.order = detail;
   });