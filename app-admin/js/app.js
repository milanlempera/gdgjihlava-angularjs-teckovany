'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('admin', [
  'ngRoute',
  'admin.filters',
  'admin.services',
  'admin.directives',
  'admin.controllers'
]);

app.config(function($routeProvider) {
  $routeProvider.
    when('/', {
      templateUrl: 'partials/list.html',
      controller: 'OrdersCtrl'
    }).
    when('/detail/:id', {
      templateUrl: 'partials/detail.html',
      controller: 'DetailCtrl',
      resolve: {
        detail: function(Orders, $route) {
          return Orders.get({id: $route.current.params.id}).$promise;
        }
      }
    })
    .otherwise({
      redirectTo: '/'
    });

});

app.constant('REST_BASE_URL', 'http://gdgangularteckovany.apiary-mock.com');
