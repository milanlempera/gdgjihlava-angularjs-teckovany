'use strict';

/* Filters */

var filters = angular.module('teckovany.filters', []);
filters.filter('colorTranslation', function(Colors) {
// TODO 1.2 return function which translates code to Colors
        return function (code) {
            return Colors.translate(code);
        };
    });
