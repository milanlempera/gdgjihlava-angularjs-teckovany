'use strict';

/* Services */

var services = angular.module('teckovany.services', []);

services.factory('Cart', function() {
    return {
        items: {},
        
        getItemsAsArray: function() {
            var items = [];
            for (var key in this.items) {
                items.push(this.items[key]);
            }

            return items;
        },
        
        addToCart: function(product, price, count) {
            // remove unwanted settings
            if (!product.hardCover) {
                delete product.text;
                delete product.textColor;
            }

            var jsonized = JSON.stringify(product);

            var items = this.items;

            if (jsonized in items) {
                items[jsonized].count += count;

            } else {
                var cartLine = {
                    product: product,
                    count: count,
                    price: price
                };

                this.items[jsonized] = cartLine;
            }
        },
        
        getPrice: function() {
            var price = 0;

            for (var key in this.items) {
                var line = this.items[key];
                price += line.count * line.price;
            }

            return price;

        },

        isEmpty: function() {
            return Object.keys(this.items).length < 1;
        },

        empty: function() {
            this.items = {};
        },

        remove: function(key) {
            delete this.items[key];
        }
    };
});

services.factory('Colors', function() {
    return {
        colorTranslations : {
            'white'         : 'Bílá',
            'bright-green'  : 'Zářivě zelená',
            'vanila'        : 'Vanilková',
            'pale-blue'     : 'Světle modrá',
            'pale-yellow'   : 'Světle žlutá',
            'light-blue'    : 'Pastelově modrá',
            'bright-yellow' : 'Zářivě žlutá',
            'blue'          : 'Modrá',
            'dark-orange'   : 'Tmavě oranžová',
            'bright-blue'   : 'Zářivě modrá',
            'light-pink'    : 'Světle růžová',
            'dark-blue'     : 'Tmavě modrá',
            'pink'          : 'Růžová',
            'dark-purple'   : 'Tmavě fialová',
            'bring-pink'    : 'Zářivě růžová',
            'brown'         : 'Hnědá',
            'red'           : 'Červená',
            'dark-brown'    : 'Tmavě hnědá',
            'dark-red'      : 'Tmavě červená',
            'grey'          : 'Šedá',
            'lime-green'    : 'Lipově zelená',
            'black'         : 'Černá'
        },

        allColors : [
            'white'         ,'bright-green'  ,
            'vanila'        ,'pale-blue'     ,
            'pale-yellow'   ,'light-blue'    ,
            'bright-yellow' ,'blue'          ,
            'dark-orange'   ,'bright-blue'   ,
            'light-pink'    ,'dark-blue'     ,
            'pink'          ,'dark-purple'   ,
            'bring-pink'    ,'brown'         ,
            'red'           ,'dark-brown'    ,
            'dark-red'      ,'grey'          ,
            'lime-green'    ,'black'
        ],

        softColors : [
            'white',        'lime-green',
            'pale-yellow',  'bright-green',
            'bright-yellow','pale-blue',
            'dark-orange',  'bright-blue',
            'pink',         'blue',
            'bring-pink',   'black',
            'red'
        ],

        translate: function(color){
            return this.colorTranslations[color];
        }
    };
});

services.service('PriceCalculator', function() {
    /**
     * @param {Object} product
     * @returns {Number}
     */
    this.calculate = function(product) {
        var price = this.getSizePrice(product.pageSize);

        if (product.hardCover) {
            price *= 1.1;
        }
        if (product.corner) {
            price += 5;
        }
        if (product.rubber) {
            price += 10;
        }

        price += this.getPagesPrice(product.numberOfPages);

        return Math.ceil(price);
    };

    /**
     * @protected
     * @param {Number} pageSize
     * @returns {Number}
     */
    this.getSizePrice = function(pageSize) {
        var price;

        switch (pageSize) {
            case 'A4':
            default:
                price = 90;
                break;
            case 'SQUARE' :
                price = 85;
                break
            case 'A5' :
                price = 80;
                break
        }

        return price;
    };

    /**
     * @protected
     * @param {Number} numberOfPages
     * @returns {Number}
     */
    this.getPagesPrice = function(numberOfPages) {
        var count = Math.ceil(numberOfPages / 5);
        return 3 * count;
    };
});
