'use strict';

/* Controllers */

var controllers = angular.module('teckovany.controllers', []);

controllers.controller('ProductCtrl', function($scope, PriceCalculator, Cart, Colors) {
    $scope.product = {
        pageSize: 'A4',
        numberOfPages: 50,
        hardCover: true,
        corner: false,
        rubber: false,
        color: "black",
        textColor: "bright-yellow",
        text: null
    };

    $scope.cart = Cart;
    $scope.colors = Colors;
    $scope.count = 1;   

    $scope.addToCart = function() {
        var clonedProduct = angular.copy($scope.product);
        var price = $scope.getPrice(clonedProduct);

        $scope.cart.addToCart(clonedProduct, price, $scope.count);
    };

    $scope.getPrice = function(product) {
        return PriceCalculator.calculate(product);
    };

    $scope.getAvailableColors = function() {
      if ($scope.product.hardCover) {
        return Colors.allColors;
      }

      return   Colors.softColors;
    };
});

controllers.controller('OrderCtrl',
    function($scope, $rootScope, Cart, REST_BASE_URL, $http) {

        $scope.order = {delivery_type : "post-prepaid"};
        $scope.cart = Cart;

        $scope.getTotalPrice = function() {
            var price = $scope.cart.getPrice();

            switch ($scope.order.delivery_type) {
                case 'post':
                    price += 90;
                    break;
                case 'post-prepaid':
                    price += 60;
                    break;
            }

            return price;
        };

        $scope.sendOrder = function() { 
            var orderData = {
                order: $scope.order,
                cart: $scope.cart.getItemsAsArray()
            };

//          TODO 3.1 use http request to send post to REST_BASE_URL + "/orders"
//          TODO 3.2 empty cart on success and set sent to rootscope
            $http.post(REST_BASE_URL + "/orders", orderData)
                .success(function(data, status, headers, config) {
                    $rootScope.sent = true;
                    Cart.empty();
                })
                .error(function (data, status, headers, config){
                    alert("Jejda, došlo k chybě při odesílání objednávky. \nOzvi se nám na info@teckovany.cz a vyřešíme to.");
                });
        };
    });