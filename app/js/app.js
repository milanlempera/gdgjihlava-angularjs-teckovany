'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('teckovany', [
  'ui.slider',
  'teckovany.filters',
  'teckovany.services',
  'teckovany.directives',
  'teckovany.controllers'
]);

app.constant('REST_BASE_URL', 'http://gdgangularteckovany.apiary-mock.com');