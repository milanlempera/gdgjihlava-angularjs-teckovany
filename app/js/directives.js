'use strict';

/* Directives */

var directives = angular.module('teckovany.directives', []);

directives.directive('ngColor', function() {
  return {
    restrict: 'A',
    scope: {
      ngModel: '=',
      options: '=ngColor'
    },
    link: function(scope, element) {

      scope.setColor = function(color) {
        scope.ngModel = color;
      }
    },
    templateUrl: 'partials/directives/color.html',
    transclude: true
  }
});
